import Header from './header'
const Container = (props)=>(
    <div>
        <Header/>
        <div className="container wrapper ">
            {
                props.children
            }
        </div>
    </div>
)
export default Container