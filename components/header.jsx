import Head from 'next/head'
const Header = ()=>(
<div>
    <Head>
    <link rel="preconnect" href="https://fonts.gstatic.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Caveat+Brush&display=swap" rel="stylesheet"/>
    </Head>
<nav className="navbar navbar-expand-lg navbar-light bg-dark-blue">
  <div className="container-fluid">
    <h3 className=" brush-font pro-brand" href="#">SUSHI-ZUCHY</h3>

    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className="navbar-nav brush-font">
        <li className="nav-item">
          <a className="nav-link text-grill" aria-current="page" href="#">COMBOS</a>
        </li>
        <li className="nav-item">
          <a className="nav-link text-grill" href="#">ROLLOS</a>
        </li>
        <li className="nav-item">
          <a className="nav-link text-grill" href="#">OFERTAS</a>
        </li>
        <li className="nav-item">
          <a className="nav-link text-grill" href="#">UBICACIÓN</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>
)
export default Header