import Container from '../components/container'


const Card = (props)=>{
  const hour = new Date().getHours()
  let msgByTime = "Hola Buenas Noches"

  if(hour >= 6 && hour <= 12){
    msgByTime = "Hola Buenas Dias"
  }
  if(hour >= 12 && hour <= 19){
    msgByTime = "Hola Buenas Tardes"
  }

  let msg = `${msgByTime}, me llamo Ana, estuve viendo su menu, y quisiera ordenar *${props.title}* por favor`
  return(
    <div className="col-sm-12 col-12 col-lg-3 mb-4">
      <div className="card" >
        <img src={props.img} className="card-img-top" alt="..."/>
        <div className="card-body">
          <h5 className="card-title">
            {props.title.slice(3)}
          </h5>
          <p className="card-text">
            {props.desc}
          </p>
        </div>
        <ul className="list-group list-group-flush">
          {
            props.ingredients.map((item)=>(
              <li className="list-group-item">{item}</li>
            ))
          }      
        </ul>
        <div className="card-body">
          <a  className="card-link h4 text-decoration-none text-success">${props.precio}</a>
          <a href={`https://api.whatsapp.com/send?phone=541127497646&text=${msg}`} target="_blank" className="card-link text-decoration-none">Hacer pedido  </a>
        </div>
      </div>  
    </div>
  )
}

const Index = ()=>{
    return (
      <Container><br/>
        <div className="row">
          
          <Card 
          img="https://i.pinimg.com/originals/f3/12/40/f312400c8bf7fae8a54189f8db7444c3.jpg" 
          title="UNA BOLA DE FUEGO" 
          desc="Deliciosa bola de arroz empanizada y rellena de camarón." ingredients={["Camarón","Tocino","Aguacate"]} 
          precio="95" />

          <Card 
          img="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/California_Sushi_%2826571101885%29.jpg/1200px-California_Sushi_%2826571101885%29.jpg" 
          title="UN CALIFORNIA ROLL" 
          desc="Sushi tradicional relleno con marisco a elección" 
          ingredients={["Queso","Aguacate","Pepino"]} 
          precio="40" />
        </div>
      </Container>
    )
}
export default Index